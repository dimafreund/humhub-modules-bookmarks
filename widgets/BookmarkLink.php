<?php

namespace humhub\modules\bookmark\widgets;

use yii\base\Widget;
use yii\helpers\Url;

class BookmarkLink extends Widget
{

    /**
     * @var \humhub\modules\content\components\ContentActiveRecord
     */
    public $object;

    public function run()
    {

        $currentUserBookmarked = false;

        return $this->render(
            'bookmarkLink',
            [
                'object' => $this->object,
                'id' => $this->object->content->id,
                'bookmarkUrl' => Url::to(
                    [
                        '/bookmark/bookmark/bookmark',
                        'contentModel' => $this->object->className(),
                        'contentId' => $this->object->id
                    ]
                ),
                'unbookmarkUrl' => Url::to(
                    [
                        '/bookmark/bookmark/unbookmark',
                        'contentModel' => $this->object->className(),
                        'contentId' => $this->object->id
                    ]
                ),
                'currentUserBookmarked' => $currentUserBookmarked,
            ]
        );
    }
}
