<?php

namespace humhub\modules\bookmark\models;

use humhub\components\behaviors\PolymorphicRelation;
use humhub\modules\content\components\ContentActiveRecord;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bookmark".
 *
 * @property integer $id
 * @property string $object_model
 * @property integer $object_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @package humhub.modules.bookmark.models
 * @since 1.0
 * @author Dmytro Tkach
 */
class Bookmark extends ContentActiveRecord
{
    protected $updateContentStreamSort = false;

    protected $automaticContentFollowing = false;

    public function behaviors()
    {
        return [
            [
                'class' => PolymorphicRelation::class(),
                'mustBeInstanceOf' => [
                    ActiveRecord::class(),
                ]
            ]
        ];
    }

    public function rules()
    {
        return array(
            array(['object_model', 'object_id'], 'required'),
            array(['id', 'object_id'], 'integer'),
        );
    }
}
