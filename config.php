<?php


use humhub\modules\content\widgets\WallEntryControls;
use humhub\modules\content\widgets\WallEntryLinks;

return [
    'id' => 'bookmark',
    'class' => 'humhub\modules\bookmark\Module',
    'namespace' => 'humhub\modules\bookmark',
    'events' => [
        [
            'class' => WallEntryLinks::class,
            'event' => WallEntryLinks::EVENT_INIT,
            'callback' => [
                'humhub\modules\bookmark\Events',
                'onWallEntryLinksInit'
            ]
        ],
    ]
];
