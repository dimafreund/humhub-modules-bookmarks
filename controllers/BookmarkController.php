<?php

namespace humhub\modules\like\controllers;

use humhub\components\behaviors\AccessControl;
use humhub\modules\bookmark\models\Bookmark;
use Yii;
use humhub\modules\content\components\ContentAddonController;

class BookmarkController extends ContentAddonController
{

    public function behaviors()
    {
        return [
            'acl' => [
                'class' => AccessControl::class,
                'guestAllowedActions' => ['show-bookmark']
            ]
        ];
    }

    public function actionBookmark()
    {
        $this->forcePostRequest();

        $bookmark = Bookmark::findOne(
            [
                'object_model' => $this->contentModel,
                'object_id' => $this->contentId,
                'created_by' => Yii::$app->user->id
            ]
        );

        if ($bookmark === null) {
            $bookmark = new Bookmark(
                [
                'object_model' => $this->contentModel,
                'object_id' => $this->contentId
                ]
            );
            $bookmark->save();
        }

        return $this->actionShowLikes();
    }

    public function actionUnbookmark()
    {
        $this->forcePostRequest();

        if (!Yii::$app->user->isGuest) {
            $bookmark = Bookmark::findOne(
                [
                'object_model' => $this->contentModel,
                'object_id' => $this->contentId,
                'created_by' => Yii::$app->user->id
                ]
            );
            if ($bookmark !== null) {
                $bookmark->delete();
            }
        }

        return true;
    }
}
