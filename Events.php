<?php

namespace humhub\modules\bookmark;

use humhub\modules\bookmark\widgets\BookmarkLink;
use yii\base\BaseObject;

class Events extends BaseObject
{
    public static function onWallEntryLinksInit($event)
    {
        $stackWidget = $event->sender;
        $content = $event->sender->object;

        $stackWidget->addWidget(BookmarkLink::class, ['object' => $content]);
    }
}
