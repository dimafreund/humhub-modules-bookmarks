<?php

namespace humhub\modules\bookmark;

class Module extends \humhub\components\Module
{

    public function disable()
    {
        foreach (models\Bookmark::find()->all() as $bookmark) {
            $bookmark->delete();
        }

        parent::disable();
    }
}
