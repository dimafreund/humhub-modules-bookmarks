<?php

namespace humhub\modules\bookmark\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BookmarkAsset extends AssetBundle
{

    public $jsOptions = ['position' => View::POS_END];

    public $sourcePath = '@bookmark/resources';

    public $js = [
        'js/humhub.resources.js'
    ];
}
